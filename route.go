package aimiddleware

import (
    "net/http"
    "time"

    "github.com/gorilla/mux"
    "github.com/sirupsen/logrus"
)

type Route struct {
    Name        string
    Method      string
    Pattern     string
    Handler     http.Handler
}

type Routes []Route

func (r Routes) Add(name, method, pattern string, handler http.Handler) Routes {
    return append(r, Route{name, method, pattern, handler})
}

func (r Routes) Func(name, method, pattern string, handler func(http.ResponseWriter, *http.Request)) Routes {
    return r.Add(
        name,
        method,
        pattern,
        http.HandlerFunc(handler),
        )
}

func NewRouter(routes Routes) *mux.Router {
    router := mux.NewRouter().StrictSlash(true)
    for _, route := range routes {
        handler := logging(route)
        router.
            Methods(route.Method).
            Path(route.Pattern).
            Name(route.Name).
            Handler(handler)
    }

    return router
}

func NewRoute(name, method, patter string, handler http.Handler) Route {
    return Route{
        name,
        method,
        patter,
        handler,
    }
}

func logging(route Route) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        start := time.Now()
        route.Handler.ServeHTTP(w, r)
        logrus.WithFields(logrus.Fields{
            "method":       r.Method,
            "pattern":      route.Pattern,
            "request-uri":  r.RequestURI,
            "name":         route.Name,
            "runtime":      time.Since(start),
        }).Info("Run route")
    })
}