package aimiddleware

import "fmt"

type MiddleWareError string

func (e MiddleWareError) Error() string {
    return fmt.Sprintf("Ai MiddleWare: Error - %s", string(e))
}