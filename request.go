package aimiddleware

import (
    "net/http"
    "strconv"
)

func getParam(r *http.Request, key string) (string, bool) {
    r.ParseForm()
    v := r.Form.Get(key)
    if v != "" {
        return v, true
    }
    return "", false
}

func GetInt(r *http.Request, key string, def int) int {
    v, ok := getParam(r, key)
    if !ok {
        return def
    }
    i, _ := strconv.Atoi(v)
    return i
}

func GetInt64(r *http.Request, key string, def int64) int64 {
    v, ok := getParam(r, key)
    if !ok {
        return def
    }
    i, _ := strconv.Atoi(v)
    return int64(i)
}

func GetStr(r *http.Request, key, def string) string {
    v, ok := getParam(r, key)
    if !ok {
        return def
    }
    return v
}

func GetHeader(r *http.Request, key, def string) string {
    v := r.Header.Get(key)
    if v == "" {
        return def
    }
    return v
}