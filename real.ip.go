package aimiddleware

import (
    "bytes"
    "net"
    "net/http"
    "strings"

    "github.com/sirupsen/logrus"
)

var cidrs []*net.IPNet

func init() {
    maxCIDRBlocks := []string{
        "127.0.0.1/8",
        "10.0.0.0/8",
        "172.16.0.0/12",
        "192.168.0.0/16",
        "169.254.0.0/16",
        "::1/128",
        "fc00::/7",
        "fe80::/10",
    }
    cidrs = make([]*net.IPNet, len(maxCIDRBlocks))
    for i, maxCIDRBlock := range maxCIDRBlocks {
        _, cidr, _ := net.ParseCIDR(maxCIDRBlock)
        cidrs[i] = cidr
    }
}

func isPrivateAddress(address string) (bool, error) {
    ipAddress := net.ParseIP(address)
    if ipAddress == nil {
        return false, MiddleWareError("address in not valid")
    }

    for i := range cidrs {
        if cidrs[i].Contains(ipAddress) {
            return true, nil
        }
    }

    return false, nil
}

func fromRequest(r *http.Request) string {
    xRealIP := r.Header.Get("X-Real-Ip")
    xForwardedFor := r.Header.Get("X-Forwarded-For")

    if xRealIP == "" && xForwardedFor == "" {
        var remoteIP string
        if strings.ContainsRune(r.RemoteAddr, ':') {
            remoteIP, _, _ = net.SplitHostPort(r.RemoteAddr)
        } else {
            remoteIP = r.RemoteAddr
        }

        return remoteIP
    }

    for _, address := range strings.Split(xForwardedFor, ",") {
        address = strings.TrimSpace(address)
        isPrivate, err := isPrivateAddress(address)
        if !isPrivate && err == nil {
            return address
        }
    }

    return xRealIP
}

func RealIP(r *http.Request) string {
    return fromRequest(r)
}

func ipInc(ip net.IP) {
    for j := len(ip) - 1; j >= 0; j-- {
        ip[j]++
        if ip[j] > 0 {
            break
        }
    }
}

func IpIsRange(ipCheck, start, end string) bool {
    trial := net.ParseIP(ipCheck)
    ipStart := net.ParseIP(start)
    ipEnd := net.ParseIP(end)
    if bytes.Compare(trial, ipStart) >= 0 && bytes.Compare(trial, ipEnd) <= 0 {
        return true
    }
    return false
}

func IpIsSubnet(ipCheck, cidr string) bool {
    ip, ipNet, err := net.ParseCIDR(cidr)
    if err != nil {
        logrus.Error(err)
    }
    var ips []string
    for ip := ip.Mask(ipNet.Mask); ipNet.Contains(ip); ipInc(ip) {
        ips = append(ips, ip.String())
    }
    return IpIsRange(ipCheck, ips[0], ips[len(ips)-1])
}