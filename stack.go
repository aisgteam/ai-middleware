package aimiddleware

import "net/http"

type Constructor func(h http.Handler) http.Handler

type StackInterface interface {
    Append(...Constructor) StackInterface
    Extend(StackInterface) StackInterface
    Extends(...StackInterface) StackInterface
    Then(http.Handler) http.Handler
    Constructors() []Constructor
}

type Stack struct {
    constructors    []Constructor
}

func (s *Stack) Append(constructors ...Constructor) StackInterface {
    s.constructors = append(s.constructors, constructors...)

    return s
}

func (s *Stack) Extend(stack StackInterface) StackInterface {
    s.Append(stack.Constructors()...)

    return s
}

func (s *Stack) Extends(stacks ...StackInterface) StackInterface {
    for _, stack := range stacks {
        s.Extend(stack)
    }

    return s
}

func (s *Stack) Then(h http.Handler) http.Handler {
    if h == nil {
        h = http.DefaultServeMux
    }
    for i := range s.constructors {
        h = s.constructors[len(s.constructors)-1-i](h)
    }

    return h
}

func (s *Stack) Constructors() []Constructor {
    return s.constructors
}

func NewStack(constructors ...Constructor) StackInterface {
    return &Stack{constructors}
}